package br.com.cavalcante.paletras.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;

import static br.com.cavalcante.paletras.shared.ConstantesTeste.CINCO_MINUTOS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.NOME_DO_ARQUIVO_INEXISTENTE;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.NOME_DO_ARQUIVO_LIGHTNING;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.NOME_DO_ARQUIVO_PROPOSALS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.NOME_DO_ARQUIVO_UM_REGISTRO;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.PALESTRA_DO_ARQUIVO_LIGHTNING;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.PALESTRA_DO_ARQUIVO_UM_REGISTRO;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.SESSENTA_MINUTOS;
import static br.com.cavalcante.paletras.shared.Utils.obterUnicoRegistroComAsserts;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ConversorArquivoParaListaDePalestrasTest {

    private ConversorArquivoParaListaDePalestras conversorArquivoParaListaDePalestras = new ConversorArquivoParaListaDePalestras();

    @Test
    public void dadoQueArquivoNaoExistaAoConverterDeveriaRetornarUmaListaVazia() throws Exception {
        List<Palestra> palestras = converter(NOME_DO_ARQUIVO_INEXISTENTE);

        assertTrue(palestras.isEmpty());
    }

    @Test
    public void dadoQueArquivoExistaAoConverterDeveriaRetornarUmaListaDePalestrasComMesmaQuantidadeDeLinhas() throws Exception {
        List<Palestra> palestras = converter(NOME_DO_ARQUIVO_PROPOSALS);

        assertNotNull(palestras);
        assertEquals(19, palestras.size());
    }

    @Test
    public void dadoQueArquivoContenhaUmRegistroComTempoAoConverterDeveriaRetornarUmaListaDePalestrasComDescricaoEDuracao() throws Exception {
        List<Palestra> palestras = converter(NOME_DO_ARQUIVO_UM_REGISTRO);

        Palestra palestra = obterUnicoRegistroComAsserts(palestras);
        assertEquals(PALESTRA_DO_ARQUIVO_UM_REGISTRO, palestra.getDescricao());
        assertEquals(SESSENTA_MINUTOS, palestra.getDuracao());
    }

    @Test
    public void dadoQueArquivoContenhaUmRegistroLightningAoConverterDeveriaRetornarUmaListaDePalestrasComDescricaoEDuracaoDeCincoMinutos() throws Exception {
        List<Palestra> palestras = converter(NOME_DO_ARQUIVO_LIGHTNING);

        Palestra palestra = obterUnicoRegistroComAsserts(palestras);
        assertEquals(PALESTRA_DO_ARQUIVO_LIGHTNING, palestra.getDescricao());
        assertEquals(CINCO_MINUTOS, palestra.getDuracao());
    }

    private List<Palestra> converter(String nomeArquivo) {
        return conversorArquivoParaListaDePalestras.converter(nomeArquivo);
    }

}