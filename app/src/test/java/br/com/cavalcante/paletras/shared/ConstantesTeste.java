package br.com.cavalcante.paletras.shared;

import java.util.List;

import br.com.cavalcante.paletras.model.Conferencia;
import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.model.Sessao;
import br.com.cavalcante.paletras.model.Trilha;

import static br.com.cavalcante.paletras.model.Periodo.MANHA;
import static br.com.cavalcante.paletras.model.Periodo.TARDE;
import static br.com.cavalcante.paletras.shared.Constantes.ALMOCO;
import static br.com.cavalcante.paletras.shared.Constantes.NOME_CONFERENCIA;
import static br.com.cavalcante.paletras.shared.Constantes.ZERO;
import static br.com.cavalcante.paletras.shared.Utils.unir;
import static com.google.common.collect.Lists.newArrayList;

public interface ConstantesTeste {

    String VAZIO = "";
    String NOME_DO_ARQUIVO_PROPOSALS = "proposals.txt";
    String NOME_DO_ARQUIVO_UM_REGISTRO = "um_registro.txt";
    String NOME_DO_ARQUIVO_LIGHTNING = "lightning.txt";
    String NOME_DO_ARQUIVO_INEXISTENTE = "teste.txt";
    String PALESTRA_DO_ARQUIVO_UM_REGISTRO = "Diminuindo tempo de execução de testes em aplicações Rails enterprise";
    String PALESTRA_DO_ARQUIVO_LIGHTNING = "Reinventando a roda em ASP clássico";
    String NOME_TRILHA_A = "Track A";
    String NOME_TRILHA_B = "Track B";

    Integer CENTO_E_OITENTA_UM = 181;
    Integer CENTO_E_OITENTA = 180;
    Integer SESSENTA_MINUTOS = 60;
    Integer TRINTA_MINUTOS = 30;
    Integer CINCO_MINUTOS = 5;

    Palestra PALESTRA_CINCO_MINUTOS = new Palestra(VAZIO, CINCO_MINUTOS);
    Palestra PALESTRA_TRINTA_MINUTOS = new Palestra(VAZIO, TRINTA_MINUTOS);
    Palestra PALESTRA_SESSENTA_MINUTOS = new Palestra(VAZIO, SESSENTA_MINUTOS);
    Palestra PALESTRA_CENTO_E_OITENTA_MINUTOS = new Palestra(VAZIO, CENTO_E_OITENTA);
    Palestra PALESTRA_CENTO_E_OITENTA_E_UM_MINUTOS = new Palestra(VAZIO, CENTO_E_OITENTA_UM);
    Palestra PALESTRA_NETWORKING = new Palestra(Constantes.PALESTRA_NETWORKING, ZERO);

    List<Palestra> LISTA_VAZIA_PALESTRAS = newArrayList();
    List<Palestra> LISTA_COM_TRES_PALESTRAS = newArrayList(PALESTRA_CINCO_MINUTOS, PALESTRA_SESSENTA_MINUTOS, PALESTRA_TRINTA_MINUTOS);
    List<Palestra> LISTA_COM_TRES_PALESTRAS_ORDENADAS = newArrayList(PALESTRA_SESSENTA_MINUTOS, PALESTRA_TRINTA_MINUTOS, PALESTRA_CINCO_MINUTOS);
    List<Palestra> LISTA_COM_DUAS_PALESTRAS = newArrayList(PALESTRA_CENTO_E_OITENTA_E_UM_MINUTOS, PALESTRA_CENTO_E_OITENTA_MINUTOS);
    List<Palestra> LISTA_COM_DUAS_PALESTRAS_SESSAO_MANHA = newArrayList(PALESTRA_CENTO_E_OITENTA_MINUTOS, ALMOCO);
    List<Palestra> LISTA_COM_DUAS_PALESTRAS_SESSAO_TARDE = newArrayList(PALESTRA_CENTO_E_OITENTA_E_UM_MINUTOS, PALESTRA_NETWORKING);
    List<Palestra> LISTA_COM_QUATRO_PALESTRAS = newArrayList(PALESTRA_CENTO_E_OITENTA_MINUTOS, ALMOCO, PALESTRA_CENTO_E_OITENTA_E_UM_MINUTOS, PALESTRA_NETWORKING);
    List<Palestra> LISTA_COM_OITO_PALESTRAS = unir(LISTA_COM_QUATRO_PALESTRAS, LISTA_COM_QUATRO_PALESTRAS);

    Sessao SESSAO_MANHA_TRILA_A = new Sessao(MANHA, LISTA_COM_DUAS_PALESTRAS_SESSAO_MANHA);
    Sessao SESSAO_TARDE_TRILHA_A = new Sessao(TARDE, LISTA_COM_DUAS_PALESTRAS_SESSAO_TARDE);
    Sessao SESSAO_MANHA_TRILA_B = new Sessao(MANHA, LISTA_COM_DUAS_PALESTRAS_SESSAO_MANHA);
    Sessao SESSAO_TARDE_TRILHA_B = new Sessao(TARDE, LISTA_COM_DUAS_PALESTRAS_SESSAO_TARDE);

    List<Sessao> LISTA_VAZIA_SESSOES = newArrayList();
    List<Sessao> LISTA_COM_DUAS_SESSOES = newArrayList(SESSAO_MANHA_TRILA_A, SESSAO_TARDE_TRILHA_A);
    List<Sessao> LISTA_COM_QUATRO_SESSOES = newArrayList(SESSAO_MANHA_TRILA_A, SESSAO_TARDE_TRILHA_A, SESSAO_MANHA_TRILA_B, SESSAO_TARDE_TRILHA_B);

    Trilha TRILA_A = new Trilha(NOME_TRILHA_A, SESSAO_MANHA_TRILA_A, SESSAO_TARDE_TRILHA_A);
    Trilha TRILA_B = new Trilha(NOME_TRILHA_B, SESSAO_MANHA_TRILA_B, SESSAO_TARDE_TRILHA_B);

    List<Trilha> LISTA_COM_DUAS_TRILHAS = newArrayList(TRILA_A, TRILA_B);

    Conferencia CONFERENCIA_COM_DUAS_TRILHAS = new Conferencia(NOME_CONFERENCIA, LISTA_COM_DUAS_TRILHAS);

}
