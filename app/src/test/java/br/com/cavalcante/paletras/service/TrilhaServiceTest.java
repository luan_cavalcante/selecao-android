package br.com.cavalcante.paletras.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import br.com.cavalcante.paletras.model.Sessao;
import br.com.cavalcante.paletras.model.Trilha;

import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_DUAS_SESSOES;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_DUAS_TRILHAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_QUATRO_SESSOES;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_VAZIA_SESSOES;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.NOME_TRILHA_A;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.SESSAO_MANHA_TRILA_A;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.SESSAO_TARDE_TRILHA_A;
import static br.com.cavalcante.paletras.shared.Utils.obterUnicoRegistroComAsserts;
import static br.com.cavalcante.paletras.shared.Utils.resetarSessoes;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrilhaServiceTest {

    @InjectMocks
    private TrilhaService trilhaService = new TrilhaService();

    @Mock
    private SessaoService sessaoServiceMock;

    @Test
    public void dadoUmaListaVaziaDeSessoesAoObterTrilhasDeveriaRetornarUmaListaVazia() throws Exception {
        aoObterSessoesRetornar(LISTA_VAZIA_SESSOES);
        List<Trilha> trilhas = trilhaService.obterTrilhas();

        assertTrue(trilhas.isEmpty());
    }

    @Test
    public void dadoUmaListaComDuasSessoesAoObterTrilhasDeveriaRetornarUmaTrilha() throws Exception {
        aoObterSessoesRetornar(LISTA_COM_DUAS_SESSOES);
        List<Trilha> trilhas = trilhaService.obterTrilhas();

        Trilha trilha = obterUnicoRegistroComAsserts(trilhas);
        assertEquals(NOME_TRILHA_A, trilha.getDescricao());
        assertEquals(SESSAO_MANHA_TRILA_A, trilha.getManha());
        assertEquals(SESSAO_TARDE_TRILHA_A, trilha.getTarde());
    }

    @Test
    public void dadoUmaListaComQuatroSessoesAoObterTrilhasDeveriaRetornarDuasTrilhas() throws Exception {
        aoObterSessoesRetornar(LISTA_COM_QUATRO_SESSOES);
        List<Trilha> trilhas = trilhaService.obterTrilhas();

        assertThat(trilhas, contains(LISTA_COM_DUAS_TRILHAS.toArray()));
    }

    private void aoObterSessoesRetornar(List<Sessao> sessoes) {
        when(sessaoServiceMock.obterSessoes()).thenReturn(resetarSessoes(sessoes));
    }

}