package br.com.cavalcante.paletras.repository.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.repository.PalestraRepository;
import br.com.cavalcante.paletras.service.ConversorArquivoParaListaDePalestras;

import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_TRES_PALESTRAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_TRES_PALESTRAS_ORDENADAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_VAZIA_PALESTRAS;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PalestraRepositoryFromFileTest {

    @InjectMocks
    private PalestraRepository palestraRepository = new PalestraRepositoryFromFile();

    @Mock
    private ConversorArquivoParaListaDePalestras conversorMock;

    @Test
    public void dadoUmaListaVaziaDePalestrasDeveriaRetornarUmaListaVazia() throws Exception {
        when(conversorMock.converter(any())).thenReturn(LISTA_VAZIA_PALESTRAS);
        List<Palestra> palestras = palestraRepository.obterTodas();

        assertTrue(palestras.isEmpty());
    }

    @Test
    public void dadoUmaListaDePalestrasDeveriaOrdenarPorMaiorDuracao() throws Exception {
        when(conversorMock.converter(any())).thenReturn(LISTA_COM_TRES_PALESTRAS);
        List<Palestra> palestras = palestraRepository.obterTodas();

        assertThat(palestras, contains(LISTA_COM_TRES_PALESTRAS_ORDENADAS.toArray()));
    }

}