package br.com.cavalcante.paletras.shared;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Function;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.model.Sessao;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public interface Utils {

    static <T> T obterUnicoRegistroComAsserts(List<T> lista) {
        assertNotNull(lista);
        assertEquals(1, lista.size());

        return lista.get(0);
    }

    static List<Palestra> resetarPalestras(List<Palestra> palestras) {
        return obterListaExecutandoAcao(palestras, palestra -> {
            palestra.setAgendada(false);
            return palestra;
        });
    }

    static List<Sessao> resetarSessoes(List<Sessao> sessoes) {
        return obterListaExecutandoAcao(sessoes, sessao -> {
            sessao.setAgendada(false);
            return sessao;
        });
    }

    static <T, R> List<R> obterListaExecutandoAcao(List<T> list, Function<T, R> acao) {
        return list.stream().map(acao).collect(toList());
    }

    static <T> List<T> unir(List<T> lista1, List<T> lista2) {
        List<T> lista = Lists.newArrayList(lista1);
        lista.addAll(lista2);
        return lista;
    }

}
