package br.com.cavalcante.paletras.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import br.com.cavalcante.paletras.model.Conferencia;
import br.com.cavalcante.paletras.model.Palestra;

import static br.com.cavalcante.paletras.shared.ConstantesTeste.CONFERENCIA_COM_DUAS_TRILHAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_DUAS_TRILHAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_OITO_PALESTRAS;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConferenciaServiceTest {

    @InjectMocks
    private ConferenciaService conferenciaService = new ConferenciaService();

    @Mock
    private TrilhaService trilhaService;

    @Test
    public void dadoUmaListaComDuasTrilhasAoOrganizarDeveriaRetornarUmaConferenciaComDuasTrilas() throws Exception {
        aoObterTrilhas();
        Conferencia conferencia = conferenciaService.organizar();

        assertNotNull(conferencia);
        assertThat(conferencia.getTrilhas(), contains(LISTA_COM_DUAS_TRILHAS.toArray()));
    }

    @Test
    public void dadoUmaConferenciaComDuasTrilhasAoObterPalestrasDeveriaRetornarUmaListaComTodasAsPalestras() throws Exception {
        aoObterTrilhas();
        List<Palestra> palestras = conferenciaService.obterPalestras(CONFERENCIA_COM_DUAS_TRILHAS);

        assertNotNull(palestras);
        assertEquals(8, palestras.size());
        assertThat(palestras, contains(LISTA_COM_OITO_PALESTRAS.toArray()));
    }

    private void aoObterTrilhas() {
        when(trilhaService.obterTrilhas()).thenReturn(LISTA_COM_DUAS_TRILHAS);
    }

}