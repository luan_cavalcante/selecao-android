package br.com.cavalcante.paletras.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.model.Sessao;
import br.com.cavalcante.paletras.repository.PalestraRepository;

import static br.com.cavalcante.paletras.model.Periodo.MANHA;
import static br.com.cavalcante.paletras.model.Periodo.TARDE;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_DUAS_PALESTRAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_DUAS_SESSOES;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_COM_TRES_PALESTRAS_ORDENADAS;
import static br.com.cavalcante.paletras.shared.ConstantesTeste.LISTA_VAZIA_PALESTRAS;
import static br.com.cavalcante.paletras.shared.Utils.obterUnicoRegistroComAsserts;
import static br.com.cavalcante.paletras.shared.Utils.resetarPalestras;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SessaoServiceTest {

    @InjectMocks
    private SessaoService sessaoService = new SessaoService();

    @Mock
    private PalestraRepository palestraRepositoryMock;

    @Test
    public void dadoUmaListaVaziaDePalestrasAoObterSessoesDeveriaRetornarUmaListaVazia() throws Exception {
        aoObterTodasAsPalestrasRetornar(LISTA_VAZIA_PALESTRAS);
        List<Sessao> sessoes = sessaoService.obterSessoes();

        assertTrue(sessoes.isEmpty());
    }

    @Test
    public void dadoDuracaoDasPalestrasParaUmaSessaoAoObterSessoesDeveriaRetornarUmaSessaoNoPeriodoDaManha() throws Exception {
        aoObterTodasAsPalestrasRetornar(LISTA_COM_TRES_PALESTRAS_ORDENADAS);
        List<Sessao> sessoes = sessaoService.obterSessoes();

        Sessao sessao = obterUnicoRegistroComAsserts(sessoes);
        assertEquals(MANHA, sessao.getPeriodo());
    }

    @Test
    public void dadoDuracaoDasPalestrasParaDuasSessoesAoObterSessoesDeveriaRetornarUmaSessaoNoPeriodoDaManhaEOutraEOutraSessaoNoPeriodoDaTarde() throws Exception {
        aoObterTodasAsPalestrasRetornar(LISTA_COM_DUAS_PALESTRAS);
        List<Sessao> sessoes = sessaoService.obterSessoes();

        assertNotNull(sessoes);
        assertEquals(2, sessoes.size());
        assertEquals(MANHA, sessoes.get(0).getPeriodo());
        assertEquals(TARDE, sessoes.get(1).getPeriodo());
    }

    @Test
    public void dadoDuracaoDasPalestrasParaDuasSessoesAoObterSessoesDeveriaRetornarSessoesComAlmocoENetworking() throws Exception {
        aoObterTodasAsPalestrasRetornar(LISTA_COM_DUAS_PALESTRAS);
        List<Sessao> sessoes = sessaoService.obterSessoes();

        assertThat(sessoes, contains(LISTA_COM_DUAS_SESSOES.toArray()));
    }

    private void aoObterTodasAsPalestrasRetornar(List<Palestra> palestras) {
        when(palestraRepositoryMock.obterTodas()).thenReturn(resetarPalestras(palestras));
    }

}