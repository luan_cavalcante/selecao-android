package br.com.cavalcante.paletras.service;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.model.Periodo;
import br.com.cavalcante.paletras.model.Sessao;
import br.com.cavalcante.paletras.repository.PalestraRepository;
import br.com.cavalcante.paletras.repository.impl.PalestraRepositoryFromFile;

import static br.com.cavalcante.paletras.model.Periodo.MANHA;
import static br.com.cavalcante.paletras.model.Periodo.TARDE;
import static br.com.cavalcante.paletras.shared.Constantes.ALMOCO;
import static br.com.cavalcante.paletras.shared.Constantes.PALESTRA_NETWORKING;
import static com.google.common.collect.Lists.newLinkedList;

public class SessaoService {

    private PalestraRepository palestraRepository;
    private Periodo periodoAnterior;

    public SessaoService() {
        palestraRepository = new PalestraRepositoryFromFile();
    }

    public List<Sessao> obterSessoes() {
        List<Palestra> palestras = palestraRepository.obterTodas();

        List<Sessao> sessoes = newLinkedList();
        Stream<Palestra> palestrasNaoAgendadas = obterPalestrasNaoAgendadas(palestras);
        for (; palestrasNaoAgendadas.count() != 0; palestrasNaoAgendadas = obterPalestrasNaoAgendadas(palestras)) {
            Periodo periodo = obterProximoPeriodo();
            Sessao sessao = organizarSessao(periodo, palestras);
            sessoes.add(sessao);
            periodoAnterior = periodo;
        }

        return sessoes;
    }

    private Sessao organizarSessao(Periodo periodo, List<Palestra> palestras) {
        Sessao sessao = new Sessao(periodo);
        obterPalestrasNaoAgendadas(palestras).forEach(palestra -> adicionarPalestra(sessao, palestra));
        adicionarAlmocoOuNetworking(sessao);

        return sessao;
    }

    private Stream<Palestra> obterPalestrasNaoAgendadas(List<Palestra> palestras) {
        return palestras.stream().filter(Palestra::naoEstaAgendada);
    }

    private void adicionarAlmocoOuNetworking(Sessao sessao) {
        Periodo periodo = sessao.getPeriodo();
        if (MANHA.equals(periodo)) {
            sessao.adicionarPalestra(ALMOCO);
        } else {
            LocalTime inicioDaPalestra = obterInicioDaPalestra(sessao);
            Palestra networking = new Palestra(PALESTRA_NETWORKING, inicioDaPalestra);
            sessao.adicionarPalestra(networking);
        }
    }

    private void adicionarPalestra(Sessao sessao, Palestra palestra) {
        if (!sessao.palestraPodeSerAdicionadaNessePeriodo(palestra)) return;

        LocalTime inicio = obterInicioDaPalestra(sessao);
        palestra.setInicio(inicio);
        sessao.adicionarPalestra(palestra);
    }

    private LocalTime obterInicioDaPalestra(Sessao sessao) {
        Periodo periodo = sessao.getPeriodo();
        return periodo.getInicio().plusMinutes(sessao.getDuracao());
    }

    private Periodo obterProximoPeriodo() {
        return periodoAnterior != null ? periodoAnterior.equals(MANHA) ? TARDE : MANHA : MANHA;
    }

}
