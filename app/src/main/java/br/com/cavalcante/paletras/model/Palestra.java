package br.com.cavalcante.paletras.model;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

import static br.com.cavalcante.paletras.shared.Constantes.ALMOCO;
import static br.com.cavalcante.paletras.shared.Constantes.VAZIO;
import static br.com.cavalcante.paletras.shared.Constantes.ZERO;
import static br.com.cavalcante.paletras.shared.DateUtils.converterLocalTimeEmStringComFormato;

public class Palestra implements Serializable {

    private String descricao;
    private Integer duracao;
    private LocalTime inicio;
    private boolean agendada;
    private Trilha trilha;

    public Palestra() {
    }

    public Palestra(String descricao, Integer duracao) {
        this.descricao = descricao;
        this.duracao = duracao;
    }

    public Palestra(String descricao, LocalTime inicio) {
        this.descricao = descricao;
        this.inicio = inicio;
        this.duracao = 0;
    }

    public Palestra(String descricao, Integer duracao, LocalTime inicio) {
        this.descricao = descricao;
        this.duracao = duracao;
        this.inicio = inicio;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public LocalTime getInicio() {
        return inicio;
    }

    public void setInicio(LocalTime inicio) {
        foiAgendada();
        this.inicio = inicio;
    }

    public boolean isAgendada() {
        return agendada;
    }

    public void setAgendada(boolean agendada) {
        this.agendada = agendada;
    }

    public Trilha getTrilha() {
        return trilha;
    }

    public void setTrilha(Trilha trilha) {
        this.trilha = trilha;
    }

    public boolean naoEstaAgendada() {
        return !agendada;
    }

    public void foiAgendada() {
        agendada = true;
    }

    public Palestra comTrilha(Trilha trilha) {
        if (!this.equals(ALMOCO)) this.setTrilha(trilha);
        return this;
    }

    public String obterDescricaoDaTrilha() {
        return trilha != null ? trilha.getDescricao() : VAZIO;
    }

    public String obterDuracaoFormatada() {
        return ZERO.equals(duracao) ? VAZIO : duracao + " Minutos";
    }

    public String obterInicioFormatado() {
        return inicio != null ? converterLocalTimeEmStringComFormato(inicio, "HH:mm") : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Palestra palestra = (Palestra) o;
        return Objects.equals(descricao, palestra.descricao) &&
                Objects.equals(duracao, palestra.duracao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descricao, duracao);
    }

}
