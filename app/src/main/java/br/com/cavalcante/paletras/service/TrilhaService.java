package br.com.cavalcante.paletras.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import br.com.cavalcante.paletras.model.Periodo;
import br.com.cavalcante.paletras.model.Sessao;
import br.com.cavalcante.paletras.model.Trilha;

import static br.com.cavalcante.paletras.model.Periodo.MANHA;
import static br.com.cavalcante.paletras.model.Periodo.TARDE;
import static com.google.common.collect.Lists.newLinkedList;

public class TrilhaService {

    private SessaoService sessaoService;
    private char caracterAnterior;

    public TrilhaService() {
        sessaoService = new SessaoService();
        caracterAnterior = 64;
    }

    public List<Trilha> obterTrilhas() {
        List<Sessao> sessoes = sessaoService.obterSessoes();

        List<Trilha> trilhas = newLinkedList();
        Stream<Sessao> sessoesNaoAgendadas = obterSessoesNaoAgendadas(sessoes);
        for (; sessoesNaoAgendadas.count() != 0; sessoesNaoAgendadas = obterSessoesNaoAgendadas(sessoes)) {
            Trilha trilha = new Trilha(obterDescricaoDaTrilha());
            Sessao manha = obterSessaoParaPeriodo(sessoes, MANHA);
            Sessao tarde = obterSessaoParaPeriodo(sessoes, TARDE);
            trilha.agendarSessoes(manha, tarde);
            trilhas.add(trilha);
        }

        return trilhas;
    }

    private String obterDescricaoDaTrilha() {
        return "Track " + (++caracterAnterior);
    }

    private Sessao obterSessaoParaPeriodo(List<Sessao> sessaos, Periodo periodo) {
        Optional<Sessao> sessao = obterSessoesNaoAgendadas(sessaos).filter(s -> s.getPeriodo().equals(periodo)).findFirst();
        return sessao.orElse(null);
    }

    private Stream<Sessao> obterSessoesNaoAgendadas(List<Sessao> palestras) {
        return palestras.stream().filter(Sessao::naoEstaAgendada);
    }

}
