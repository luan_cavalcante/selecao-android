package br.com.cavalcante.paletras.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.cavalcante.paletras.R;
import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.viewholde.PalestraViewHolder;

public class PalestraAdapter extends RecyclerView.Adapter<PalestraViewHolder> {

    private final List<Palestra> palestras;
    private final Context context;

    public PalestraAdapter(List<Palestra> palestras, Context context) {
        this.palestras = palestras;
        this.context = context;
    }

    @Override
    public PalestraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItemPalestra = LayoutInflater.from(context).inflate(R.layout.item_palestra, parent, false);
        return new PalestraViewHolder(viewItemPalestra);
    }

    @Override
    public void onBindViewHolder(PalestraViewHolder palestraViewHolder, int position) {
        Palestra palestra = palestras.get(position);
        palestraViewHolder.bind(palestra);
    }

    @Override
    public int getItemCount() {
        return palestras.size();
    }

}


