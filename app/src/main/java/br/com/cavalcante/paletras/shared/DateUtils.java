package br.com.cavalcante.paletras.shared;

import java.time.LocalTime;

import static java.time.format.DateTimeFormatter.ofPattern;

public interface DateUtils {

    static String converterLocalTimeEmStringComFormato(LocalTime data, String formato) {
        return ofPattern(formato).format(data);
    }

}
