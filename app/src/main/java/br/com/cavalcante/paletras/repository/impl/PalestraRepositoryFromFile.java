package br.com.cavalcante.paletras.repository.impl;

import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.repository.PalestraRepository;
import br.com.cavalcante.paletras.service.ConversorArquivoParaListaDePalestras;

import static br.com.cavalcante.paletras.shared.Constantes.NOME_DO_ARQUIVO;

public class PalestraRepositoryFromFile implements PalestraRepository {

    private ConversorArquivoParaListaDePalestras conversor;

    public PalestraRepositoryFromFile() {
        conversor = new ConversorArquivoParaListaDePalestras();
    }

    @Override
    public List<Palestra> obterTodas() {
        List<Palestra> palestras = conversor.converter(NOME_DO_ARQUIVO);
        palestras.sort((p1, p2) -> p2.getDuracao().compareTo(p1.getDuracao()));
        return palestras;
    }

}
