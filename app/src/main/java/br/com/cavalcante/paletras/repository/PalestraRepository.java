package br.com.cavalcante.paletras.repository;

import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;

public interface PalestraRepository {

    List<Palestra> obterTodas();

}
