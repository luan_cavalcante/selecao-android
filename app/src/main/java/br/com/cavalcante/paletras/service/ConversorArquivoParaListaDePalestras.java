package br.com.cavalcante.paletras.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.shared.Conversor;

import static br.com.cavalcante.paletras.shared.Constantes.DURACAO_PALESTRA_LIGHTNING;
import static br.com.cavalcante.paletras.shared.Constantes.ESPACO;
import static br.com.cavalcante.paletras.shared.Constantes.PALESTRA_LIGHTNING;
import static br.com.cavalcante.paletras.shared.Constantes.VAZIO;
import static com.google.common.collect.Lists.newLinkedList;

public class ConversorArquivoParaListaDePalestras implements Conversor<String, List<Palestra>> {

    @Override
    public List<Palestra> converter(String nomeArquivo) {
        InputStream arquivo = obterArquivoComStream(nomeArquivo);
        return obterArquivoComoPalestras(arquivo);
    }

    private List<Palestra> obterArquivoComoPalestras(InputStream arquivo) {
        try {
            return obterConteudoArquivo(arquivo);
        } catch (Exception e) {
            return newLinkedList();
        }
    }

    private List<Palestra> obterConteudoArquivo(InputStream arquivo) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(arquivo));
        List<Palestra> linhas = newLinkedList();
        for (String linha = reader.readLine(); linha != null; linha = reader.readLine()) {
            Palestra palestra = converterLinhaParaPalestra(linha);
            linhas.add(palestra);
        }
        return linhas;
    }

    private Palestra converterLinhaParaPalestra(String linha) {
        String descricao = obterDescricao(linha);
        Integer duracao = obterDuracao(linha);
        return new Palestra(descricao, duracao);
    }

    private String obterDescricao(String linha) {
        String ultimaPalavra = obterUltimaPalavra(linha);
        return linha.replace(ultimaPalavra, VAZIO).trim();
    }

    private Integer obterDuracao(String linha) {
        String ultimaPalavra = obterUltimaPalavra(linha);
        return PALESTRA_LIGHTNING.equals(ultimaPalavra) ? DURACAO_PALESTRA_LIGHTNING : converterEmMinutos(ultimaPalavra);
    }

    private String obterUltimaPalavra(String linha) {
        String[] palavras = linha.split(ESPACO);
        return palavras[palavras.length - 1];
    }

    private Integer converterEmMinutos(String ultimaPalavra) {
        return Integer.valueOf(ultimaPalavra.replaceAll("\\D", VAZIO));
    }

    private InputStream obterArquivoComStream(String nomeDoArquivo) {
        return this.getClass().getClassLoader().getResourceAsStream(nomeDoArquivo);
    }

}
