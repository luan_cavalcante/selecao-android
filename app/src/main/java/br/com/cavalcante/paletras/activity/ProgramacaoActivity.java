package br.com.cavalcante.paletras.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;

import java.util.List;

import br.com.cavalcante.paletras.R;
import br.com.cavalcante.paletras.adapter.PalestraAdapter;
import br.com.cavalcante.paletras.model.Conferencia;
import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.service.ConferenciaService;

public class ProgramacaoActivity extends AppCompatActivity {

    private final ConferenciaService conferenciaService;

    public ProgramacaoActivity() {
        conferenciaService = new ConferenciaService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programacao);

        RecyclerView programacaoView = findViewById(R.id.programacao_view);
        configurarView(programacaoView);
    }

    private void configurarView(RecyclerView programacaoView) {
        Conferencia conferencia = conferenciaService.organizar();
        List<Palestra> palestras = conferenciaService.obterPalestras(conferencia);
        Adapter palestraAdapter = new PalestraAdapter(palestras, this);

        programacaoView.setHasFixedSize(true);
        programacaoView.setLayoutManager(new LinearLayoutManager(this));
        programacaoView.setAdapter(palestraAdapter);
    }

}
