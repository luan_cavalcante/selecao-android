package br.com.cavalcante.paletras.shared;

import br.com.cavalcante.paletras.model.Palestra;

import static java.time.LocalTime.of;

public interface Constantes {

    String NOME_DO_ARQUIVO = "proposals.txt";
    String NOME_CONFERENCIA = "Conferência Teste";
    String VAZIO = "";
    String ESPACO = " ";
    String PALESTRA_LIGHTNING = "lightning";
    String PALESTRA_NETWORKING = "Evento de Networking";

    Integer ZERO = 0;
    Integer DURACAO_PALESTRA_LIGHTNING = 5;

    Palestra ALMOCO = new Palestra("Almoço", of(12, 0));

}
