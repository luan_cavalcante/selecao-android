package br.com.cavalcante.paletras.shared;

public interface Conversor<R, W> {

    W converter(R fonte);

}
