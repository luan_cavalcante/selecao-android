package br.com.cavalcante.paletras.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import br.com.cavalcante.paletras.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button programacao = findViewById(R.id.botao_programacao);
        programacao.setOnClickListener(view -> {
            Intent intent = new Intent(this, ProgramacaoActivity.class);
            startActivity(intent);
        });
    }

}
