package br.com.cavalcante.paletras.service;

import java.util.List;

import br.com.cavalcante.paletras.model.Conferencia;
import br.com.cavalcante.paletras.model.Palestra;
import br.com.cavalcante.paletras.model.Trilha;

import static br.com.cavalcante.paletras.shared.Constantes.NOME_CONFERENCIA;
import static com.google.common.collect.Lists.newLinkedList;
import static java.util.stream.Collectors.toList;

public class ConferenciaService {

    private TrilhaService trilhaService;

    public ConferenciaService() {
        trilhaService = new TrilhaService();
    }

    public Conferencia organizar() {
        List<Trilha> trilhas = trilhaService.obterTrilhas();

        return new Conferencia(NOME_CONFERENCIA, trilhas);
    }

    public List<Palestra> obterPalestras(Conferencia conferencia) {
        List<Palestra> palestrasDaConferencia = newLinkedList();
        for (Trilha trilha : conferencia.getTrilhas()) {
            List<Palestra> palestrasDaTrilha = obterPalestrasDaTrilha(trilha);
            palestrasDaConferencia.addAll(palestrasDaTrilha);
        }
        return palestrasDaConferencia;
    }

    private List<Palestra> obterPalestrasDaTrilha(Trilha trilha) {
        return trilha.obterSessoes().stream()
                .map(sessao -> sessao.obterPalestrasSetandoTrilha(trilha)).flatMap(List::stream)
                .collect(toList());
    }

}
