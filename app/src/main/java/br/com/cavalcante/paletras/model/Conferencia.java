package br.com.cavalcante.paletras.model;

import java.io.Serializable;
import java.util.List;

public class Conferencia implements Serializable {

    private String descricao;
    private List<Trilha> trilhas;

    public Conferencia(String descricao, List<Trilha> trilhas) {
        this.descricao = descricao;
        this.trilhas = trilhas;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Trilha> getTrilhas() {
        return trilhas;
    }

    public void setTrilhas(List<Trilha> trilhas) {
        this.trilhas = trilhas;
    }

}
