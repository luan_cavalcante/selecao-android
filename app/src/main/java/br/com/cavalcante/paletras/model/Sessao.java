package br.com.cavalcante.paletras.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static com.google.common.collect.Lists.newLinkedList;

public class Sessao implements Serializable {

    private Periodo periodo;
    private List<Palestra> palestras;
    private Integer duracao;
    private boolean agendada;

    public Sessao(Periodo periodo) {
        this.periodo = periodo;
        palestras = newLinkedList();
        duracao = 0;
    }

    public Sessao(Periodo periodo, List<Palestra> palestras) {
        this(periodo);
        this.palestras = palestras;
        this.duracao = palestras.stream().mapToInt(Palestra::getDuracao).sum();
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public List<Palestra> getPalestras() {
        return palestras;
    }

    public void setPalestras(List<Palestra> palestras) {
        this.palestras = palestras;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public boolean isAgendada() {
        return agendada;
    }

    public void setAgendada(boolean agendada) {
        this.agendada = agendada;
    }

    public boolean naoEstaAgendada() {
        return !agendada;
    }

    public void foiAgendada() {
        agendada = true;
    }

    public void adicionarPalestra(Palestra palestra) {
        atualizarDuracao(palestra);
        palestras.add(palestra);
    }

    public boolean palestraPodeSerAdicionadaNessePeriodo(Palestra palestra) {
        return (this.duracao + palestra.getDuracao()) <= periodo.getDuracaoEmMinutos();
    }

    private void atualizarDuracao(Palestra palestra) {
        this.duracao += palestra.getDuracao();
    }

    public List<Palestra> obterPalestrasSetandoTrilha(Trilha trilha) {
        palestras.forEach(palestra -> palestra.comTrilha(trilha));
        return palestras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sessao sessao = (Sessao) o;
        return periodo == sessao.periodo &&
                Objects.equals(palestras, sessao.palestras) &&
                Objects.equals(duracao, sessao.duracao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(periodo, palestras, duracao);
    }

}
