package br.com.cavalcante.paletras.model;

import java.time.LocalTime;

import static java.time.LocalTime.of;

public enum Periodo {

    MANHA(9, 12, 180),
    TARDE(13, 17, 240);

    private final LocalTime inicio;
    private final LocalTime fim;
    private final Integer duracaoEmMinutos;

    Periodo(Integer inicio, Integer fim, Integer duracaoEmMinutos) {
        this.inicio = of(inicio, 0);
        this.fim = of(fim, 0);
        this.duracaoEmMinutos = duracaoEmMinutos;
    }

    public LocalTime getInicio() {
        return inicio;
    }

    public LocalTime getFim() {
        return fim;
    }

    public Integer getDuracaoEmMinutos() {
        return duracaoEmMinutos;
    }

}
