package br.com.cavalcante.paletras.viewholde;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.cavalcante.paletras.R;
import br.com.cavalcante.paletras.model.Palestra;

public class PalestraViewHolder extends RecyclerView.ViewHolder {

    private final TextView trilha;
    private final TextView descricao;
    private final TextView duracao;
    private final TextView inicio;

    public PalestraViewHolder(View itemView) {
        super(itemView);
        trilha = itemView.findViewById(R.id.item_trilha);
        descricao = itemView.findViewById(R.id.item_descricao);
        duracao = itemView.findViewById(R.id.item_duracao);
        inicio = itemView.findViewById(R.id.item_inicio);
    }

    public void bind(Palestra palestra) {
        trilha.setText(palestra.obterDescricaoDaTrilha());
        descricao.setText(palestra.getDescricao());
        duracao.setText(palestra.obterDuracaoFormatada());
        inicio.setText(palestra.obterInicioFormatado());
    }

}
