package br.com.cavalcante.paletras.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static com.google.common.collect.Lists.newArrayList;

public class Trilha implements Serializable {

    private String descricao;
    private Sessao manha;
    private Sessao tarde;

    public Trilha(String descricao) {
        this.descricao = descricao;
    }

    public Trilha(String descricao, Sessao manha, Sessao tarde) {
        this.descricao = descricao;
        this.manha = manha;
        this.tarde = tarde;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Sessao getManha() {
        return manha;
    }

    public void setManha(Sessao manha) {
        this.manha = manha;
    }

    public Sessao getTarde() {
        return tarde;
    }

    public void setTarde(Sessao tarde) {
        this.tarde = tarde;
    }

    public void agendarSessoes(Sessao manha, Sessao tarde) {
        manha.foiAgendada();
        tarde.foiAgendada();

        this.manha = manha;
        this.tarde = tarde;
    }

    public List<Sessao> obterSessoes() {
        return newArrayList(manha, tarde);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trilha trilha = (Trilha) o;
        return Objects.equals(descricao, trilha.descricao) &&
                Objects.equals(manha, trilha.manha) &&
                Objects.equals(tarde, trilha.tarde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descricao, manha, tarde);
    }

}
